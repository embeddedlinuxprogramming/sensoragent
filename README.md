# Virtual Sensor Agent

## Concept

Event-driven simulation, with ticks that represent minutes.
It comprises:
- An agent class that generates sensor data (one per tick), and sends it to the corresponding border router, along with the agent UUID and timestamp. The agent has an associate dropout probability, which simulates distance, noise and collision probabilities.
- A border router class that recieves data through an input queue of agents. The incoming data is passed-through to a remote application that appends it to a database.
- Every agent and border router should reside in a separate thread.

## Tests

- Configure the border router output protocol as plain text TCP (simplest possible). 
  - The aim is to test the memory and CPU behavior as the database grows.
