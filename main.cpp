#include <iostream>
#include <thread>
#include <chrono>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <pqxx/pqxx>
#include <random>

using namespace std;
using namespace std::chrono;
using namespace std::literals;

int main(int argc, char*argv[]) {
    if (argc != 10) {
        cout << "\n\rProgram usage:\n\r     " << argv[0] << "  UUID  TYPE " <<
            " TIMESTAMP_IN_UTC  HOST  DATABASE  USER  PASSWORD  COUNT\n\n\r";
    }
    else {
        // Parameter parsing.
        string uuid = argv[1];
        string type = argv[2];
        string host = argv[5];
        string database = argv[6];
        string user = argv[7];
        string passwd = argv[8];
        int N_dat = atoi(argv[9]);

        // Time stamp parsing.
        tm time_stamp {};
        string time_stamp_str = argv[3];
        (time_stamp_str += " ") += argv[4];
        cout << time_stamp_str << "\n\r";
        istringstream iss(time_stamp_str);
        iss >> get_time(&time_stamp, "%Y-%m-%d %H:%M:%S");

        if (!iss.fail()) {
            // Random number generation for value alternation.
            random_device dev;
            mt19937 rng(dev());
            uniform_int_distribution<mt19937::result_type> dist6(0,65535);

            // We use a time point to increase the minutes one at a time.
            auto tp = system_clock::from_time_t(mktime(&time_stamp));

            // Connection establishment.
            pqxx::connection c{"postgresql://" + user + ":" + passwd + "@" + host + "/" + database };
            pqxx::work txn{c};

            time_t time_date;
            tm * ptm;
            char buffer[32];
            uint16_t value;

            auto start = high_resolution_clock::now();
            int cnt = 0;
            for (int i=0; i<N_dat; ++i) {
                value = dist6(rng);
                cnt++;
                // Uncomment the following line if you want to insert a delay between transactions.
                //this_thread::sleep_for(seconds(1));
                tp += 1min;
                time_date =  system_clock::to_time_t(tp );

                // tm is used to help in converting from time point to string.
                ptm = localtime(&time_date);
                strftime(buffer, 32, "%Y-%m-%d %H:%M:%S", ptm);

                string query_msg = "INSERT INTO medidas (uuid, tipo, valor, marca) "
                                   "VALUES (" + txn.quote(uuid) + ", " + txn.quote(type) +
                                   ", " + std::to_string(value) + ", TIMESTAMP " +
                                   txn.quote(buffer) + ")";
                //cout << query_msg << "\n\r";
                pqxx::result r { txn.exec(query_msg) };
                if (cnt % 1000==0) {
                    auto stop = high_resolution_clock::now();
                    auto elapsed = duration_cast<duration<double>>(stop-start);
                    cout << "1000 items processed. Time: " << elapsed.count() << "\n\r";
                    start = stop;
                    cnt = 0;
                }
            }
            txn.commit();
        }
        else {
            cout << "Error al procesar la marca de tiempo. Abortando.\n\r";
        }
    }
    return 0;
}
